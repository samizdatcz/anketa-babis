title: "Nejisté znovuvydání Babiše a Faltýnka. Pro zvedne ruku 91 poslanců, klíčové budou hlasy Okamury a spol."
perex: "<ul class='text-md--m text-md'><li>Pro vydání Andreje Babiše a Jaroslava Faltýnka z hnutí ANO by v nové Poslanecké sněmovně zvedlo ruku 91 zákonodárců.</li><li>Vyplývá to z exkluzivní ankety serveru iROZHLAS.cz, který oslovil všech 200 členů dolní komory tak, jak usedne v novém povolebním složení.</li><li>Osud hlavních mužů hnutí ANO podle všeho závisí na hlasování Tomia Okamury a jeho strany Svoboda a přímá demokracie. Sám Okamura na dotaz redakce nereagoval, místopředseda Radim Fiala nebyl k zastižení.</li><li>Policie už dříve Babiše i Faltýnka obvinila v dotační kauze Čapího hnízda.</li></ul>"
authors: ["Hana Mazancová", "Kristýna Novotná"]
published: "15. listopadu 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/p201710220369501_171022-144141_haf.jpeg
coverimg_note: "Předseda hnutí ANO Andrej Babiš a místopředseda hnutí Jaroslav Faltýnek při příchodu do sněmovny na jednání se STAN | Foto: ČTK" 
styles: []
libraries: ["jquery"] 
options: noheader
---

Pokud bude policie chtít i nadále stíhat poslance hnutí ANO Andreje Babiše a Jaroslava Faltýnka, bude muset opět požádat Poslaneckou sněmovnu o jejich vydání. Ačkoli bývalá sněmovna dva hlavní muže hnutí při minulém hlasování k trestnímu stíhání v dotační aféře Čapího hnízda vydala, imunitu získali oba politici opětovným zvolením.

Zatím nebylo jasné, jak by nové hlasování o vydání Babiše a Faltýnka dopadlo. Proto server iROZHLAS.cz oslovil všech 200 poslanců a poslankyň nové sněmovny s dotazem, jak budou hlasovat v případě, že do dolní komory nová žádost o vydání Babiše a Faltýnka dorazí.

Pro vydání by se vyslovilo pouze 91 poslanců, 55 jich prý zatím není rozhodnuto, drtivá většina z nich je právě z Babišova hnutí. Čtyřiačtyřicet poslanců pak na dotaz serveru iROZHLAS.cz nereagovalo. Jednoznačný postoj, že budou hlasovat proti vydání, pak vyslovilo deset poslanců – devět z hnutí ANO.

<wide>
<p><strong><label for="vyber">Ze&nbsp;<span id="soucet1">200</span>&nbsp;poslanců zvolených&nbsp;za</label>
<select id="vyber">
  <option value="vsechny" selected>všechny strany</option>
  <option value="ANO">ANO</option>
  <option value="ODS">ODS</option>
  <option value="Piráti">Piráty</option>
  <option value="SPD">SPD</option>
  <option value="KSČM">KSČM</option>
  <option value="ČSSD">ČSSD</option>
  <option value="KDU-ČSL">KDU-ČSL</option>
  <option value="TOP 09">TOP 09</option>
  <option value="STAN">STAN</option>
</select> by&nbsp;<span style="color: green;"><span id="soucet2">91</span>&nbsp;hlasovalo&nbsp;pro&nbsp;vydání&nbsp;Babiše&nbsp;a&nbsp;Faltýnka</span>, <span style="color: red"><span id="soucet3">10</span>&nbsp;proti&nbsp;jejich&nbsp;vydání</span>, <span style="color: grey"><span id="soucet4">55</span>&nbsp;neví</span> a&nbsp;<span id="soucet5">44</span>&nbsp;na&nbsp;otázku&nbsp;neodpovědělo.</strong> Poslanci, kteří svou odpověď zdůvodnili, jsou označeni takto: (…) Jejich celé odpovědi zobrazíte kliknutím.</p>
<div id="anketa"></div>
</wide>

Pro vydání zvedne ruku všech 22 poslanců České pirátské strany. Ústy předsedy Ivana Bartoše to strana avizovala hned po prvním jednání poslaneckého klubu. „My jsme se v rámci našeho poslaneckého klubu jednohlasně shodli, že budeme hlasovat pro vydání,” uvedl tehdy Bartoš na tiskové konferenci. „Ať to policie vyšetří, nechceme dělat soudce ani policii,“ dovysvětlil pro server iROZHLAS.cz pirátský poslanec Martin Jiránek.

V exkluzivní anketě se pak jednoznačně pro vydání vyslovili i téměř všichni poslanci TOP 09 a Starostů a nezávislých. Pouze nováček ve sněmovně Jana Krutáková (STAN) uvedla, že ještě nemá jasno. „Všechno je čerstvé, teprve se se vším seznamuji, tak daleko jsem nepřemýšlela,“ vysvětlila Krutáková.

Spíše pro vydání je také poslanecký klub KSČM. Například Miroslav Grebeníček nechce „suplovat policii“. „To mně nepřísluší,“ říká. Jeho kolegové pak ještě doplňují, že záleží na tom, jestli se nezměnily okolnosti policejního stíhání obou politiků.

„Bylo sděleno obvinění, byla podána stížnost, tak pravděpodobně ta nová žádost bude asi obsahovat i reakci na tyto stížnosti. Já to nevím. Odpovím ano, nebo ne, až se s tím seznámím,“ řekl například předseda komunistů Vojtěch Filip.

##Ochrana Poslanecké sněmovny

Z patnácti poslanců sociální demokracie v anketě i přes opakované snahy nereagovalo nejužší vedení strany, tedy dosluhující premiér Bohuslav Sobotka a ministr vnitra Milan Chovanec. Odpověď redakce nezískala ani od nově zvoleného poslanec a současného náměstka pražské primátorky Petra Dolínka. Jasno má naopak ministr zahraničí Lubomír Zaorálek. „Pro vydání,“ odpověděl bez dalšího.

Proti vydání Babiše a Faltýnka bude z poslaneckého klubu lidovců hlasovat Stanislav Juránek. „Ještě nikdy jsem nehlasoval pro vydání žádného senátora, když jsem byl členem Senátu. A nemám v plánu hlasovat pro vydání kohokoli, protože myslím, že ochrana, která je, není ochranou lidí, ale ochranou orgánu, tedy Poslanecké sněmovny,“ vysvětlil své rozhodnutí. Jeho stranická kolegyně Pawla Golasowská ani poslanec Jiří Mihola na dotaz nereagovali. Pro vydání zvedne ruku také většina poslanců ODS.

##Nastudujeme a uvidíme

Samotní poslanci hnutí ANO pak shodně v anketě říkají: Ještě nejsme rozhodnuti. Nejlépe to ilustruje například odpověď poslance hnutí Milana Ferance: „Zeptám se vás: 5. února si vezmete, když budete venku, kožich, nebo lehký plášť? Já nevím, žádost jsem neviděl, takže vyjadřovat se k něčemu je zbytečné.“ I podle poslankyně Taťány Malé je tato otázka předčasná. „V první řadě bych se velmi ráda seznámila se skutečnými důvody, které by byly pro vydání. Z těch informací, které jsem do této chvíle měla, tak to ve mně vzbuzuje velké pochybnosti. Podklady jsem ale neviděla, takže bych se rozhodla až ve chvíli, kdy bych měla k dispozici materiály,“ říká.

Přesto se pár z těch, kteří budou hlasovat pro vydání svého šéfa a jeho pravé ruky, našlo. Ruku pro zbavení imunity zvedne čerstvý poslanec Radek Zlesák nebo Pavel Staněk. „Už tím, že pan Babiš i pan Faltýnek hlasovali pro svoje vlastní vydání, tak dali jasně najevo, že věří v nezávislost soudů a celého procesu, který je s tím spojený. Já bych se k tomu přidal,“ uvedl v anketě.

Souhlasí i další, ale zmiňují podmínky. „Myslím si, že by měli mít možnost se obhájit před soudy. Tady je ale otázka další komunikace. Trochu se bojím, že tyto věci se zneužívají. Sám zažívám na vlastní kůži, co to je zneužití takovýchto nástrojů, ať už jsou to média, nebo orgány činné v trestním řízení,“ říká nováček Ivo Vondrák. Byl by tedy pro vydání? „Ano, ale je to otázka dohody na klubu poslaneckém, takže v tom nejsem úplně vyhraněný,“ uzavřel.

##Co na to okamurovci
Osud Babiše a Faltýnka tak nyní de facto závisí na Tomiu Okamurovi a jeho hnutí. Z 22 poslanců Svobody a přímé demokracie však v anketě serveru iROZHLAS.cz odpovědělo pouze pět, přičemž jen dva zvednou ruku pro vydání. Sám Okamura na dotaz redakce nereagoval, místopředseda Radim Fiala nebyl k zastižení.
 
„Jelikož už se o tom jednou hlasovalo a naši poslanci hlasovali pro vydání, myslím, že se to nebude měnit a samozřejmě celý náš klub bude hlasovat pro vydání, jelikož už tak jednou bylo hlasováno,“ předpovídá poslankyně Tereza Hyťhová. „Budu hlasovat pro vydání. Zatím je to názor celé SPD a na tom se pravděpodobně nic měnit nebude,“ doplňuje ji stranický kolega Pavel Jelínek.
 
Jejich slova nicméně mírní jejich zkušenější kolega, poslanec Jaroslav Holík. „Jednotně s poslaneckým klubem SPD. Klub tuto situaci zatím neřešil,“ odpověděl na dotaz, jak bude o případném vydání hlasovat.
 
Sám předseda SPD nicméně před prvním hlasováním o vydání pro iROZHLAS.cz řekl, že ruku pro zvedne. „Budeme postupovat v souladu s doporučením mandátového a imunitního výboru, jehož členové se jako jediní měli možnost ve sněmovně seznámit se spisem. Kauzu by měl primárně řešit soud a policie, a nikoli sněmovna, která se tím už několik měsíců zabývá místo práce na potřebných zákonech,“ napsal tehdy Okamura (více si můžete [přečíst ZDE](https://www.irozhlas.cz/zpravy-domov/vydat-nebo-nevydat-babise-s-faltynkem-zadni-zmetci-se-nemaji-schovavat-za_1709051800_hm)).