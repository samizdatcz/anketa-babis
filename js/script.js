function Rendruj(data) {
  $("#anketa" ).empty();
  $(data).each(function(i) {
      if (this.o1.length>0) {$("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa")};
      if (this.o1.length>0) {$(".respondent").last().append("<div class='cedulka'>Klikněte pro celou odpověď</strong></div>")};
      if (this.o1.length>0 && this.v.indexOf("(…)")==-1) {this.v = this.v + " (…)"};
      if (this.o1.length==0) {$("<div class='respondent neodpovedel'></div>").appendTo("#anketa")};
      $(".respondent").last().append("<img class='portret' src='https://samizdat.blob.core.windows.net/storage/anketa-poslanci-dos/" + this.f + "'>");
      $(".respondent").last().append("<p class='jmeno'><strong>" + this.j + " " + this.p + "</strong>, " + this.s + "</p>");
      if (this.v.length>0) {$(".respondent").last().append("<p class='veta'>" + this.v + "</p>")};
      if (this.v=="Pro vydání" || this.v=="Pro vydání (…)") {$(".veta").last().css("background-color", "green")};
      if (this.v=="Proti vydání" || this.v=="Proti vydání (…)") {$(".veta").last().css("background-color", "red")};
      if (this.v=="Nevím" || this.v=="Nevím (…)") {$(".veta").last().css("background-color", "grey")};
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><p><strong>" + this.j + " " + this.p + "</strong>, " + this.s + "</p><p><em>Jak budete hlasovat o případné další policejní žádosti o vydání Andreje Babiše a Jaroslava Faltýnka k trestnímu stíhání?</em></p><p>" + this.o1 + "</p></div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        $(this).find(".cedulka").hide();
      }
    );
    
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
}

$(document).ready(function() {
  $.getJSON( "https://interaktivni.rozhlas.cz/data/anketa-babis/data/data.json", function(data) { 
    Rendruj(data);   

    $("#vyber").on("change", function() {
       var strana = this.value;
       var data2 = $.grep(data, function(n, i) {
            if (strana=="vsechny") {return(data)} else {return(n.s.indexOf(strana)!==-1)}
       });
       Rendruj(data2);
       $("#soucet1").text(data2.length);
       $("#soucet2").text($.grep(data2, function(n, i) {return(n.v=="Pro vydání" || n.v=="Pro vydání (…)")}).length);
       $("#soucet3").text($.grep(data2, function(n, i) {return(n.v=="Proti vydání" || n.v=="Proti vydání (…)")}).length);
       $("#soucet4").text($.grep(data2, function(n, i) {return(n.v=="Nevím" || n.v=="Nevím (…)")}).length);
       $("#soucet5").text($.grep(data2, function(n, i) {return(n.v=="Bez odpovědi" || n.v=="Bez odpovědi (…)")}).length);
    });
  });
});